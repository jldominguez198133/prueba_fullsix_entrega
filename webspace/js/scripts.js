// Funciones para añadir y eliminar clases
function addClass(elements, className) {
	for (var i = 0; i < elements.length; i++) {
		var element = elements[i];
		if (element.classList) {
			element.classList.add(className);
		} else {
			element.className += ' ' + className;
		}
	}
}

function removeClass(elements, className) {
	for (var i = 0; i < elements.length; i++) {
		var element = elements[i];
		if (element.classList) {
			element.classList.remove(className);
		} else {
			element.className = element.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
		}
	}
}
// Vars principal
var count = 0,
    tc = 0.5,
    pNeg = '-=20',
    pPos = '+=20',
    int = 0.2,
    buffer = $('.spinner-content'),
    card = $('.tarjeta'),
    secVentajas = $('.ventajas'),
    secVentajasH = secVentajas.height(),
    modsVentajas = $('.ventajas .flex-container > *'),
    TLMuestra;

// Animación Buffer
var TLBuffer = new TimelineMax();
    TLBuffer
        .to(buffer, tc, { opacity: 0, display: 'none', delay: 2, onComplete: function () {
            if (!mobilecheck() && $(window).width() > 768) {
                // Colocamos elementos
                TweenMax.set(modsVentajas, {marginTop: secVentajasH});
                // Comienza animación
                compruebaPos();
            }
        } });

// Animación tarjeta
// Evento click sobre el elemento tarjeta
card.click(function () {
    if (!mobilecheck() && $(window).width() > 768) {
        if ($(this).attr('class').indexOf('fixed') > -1) {
            console.log('card')
            TweenMax.to($('html, body'), tc, {scrollTop: secVentajas.offset().top});
        }
    }
});

function compruebaPos() {

    if (card.offset().top >= secVentajas.offset().top - 48) {
        card.removeClass('fixed');
        card.addClass('no-fixed');
        showMods();
    }

    $(window).scroll(function () {
        if (card.offset().top >= secVentajas.offset().top - 48) {
            card.removeClass('fixed');
            card.addClass('no-fixed');
            if (count == 0) {
                showMods();
            }
        }

        if ($(window).scrollBottom() > $('footer').height() + secVentajasH + 90) {
            card.removeClass('no-fixed');
            card.addClass('fixed');
            hideMods();
        }
    });
}

function showMods() {
    TLMuestra = new TimelineMax({});

    TLMuestra
        .staggerFromTo(modsVentajas, tc, { opacity: 0, marginTop: secVentajasH }, { opacity: 1, marginTop: 0, ease: Back.easeOut }, int);

    count++;
}
function hideMods() {
    modsVentajas.css('margin-top', secVentajasH);
    count = 0;
}

$.fn.scrollBottom = function() {
    return $(document).height() - this.scrollTop() - this.height();
};

// Animación cuadrados hover
var elemsAnimHover = $('.destacados [class*="__img"]'),
    numCubes = 6;

elemsAnimHover.each(function () {
    $(this).mouseenter(function() {
        addCubes($(this));
    });
    $(this).mouseleave(function() {
        deleteCubes($(this));
    });
});

var enX,
    enY;

function addCubes(parent) {
    //if (parent.attr('class').indexOf('slick-slide') > -1) numCubes = 8;

    var WH,
        arrayCubes = []
        params = {
            h: '',
            w: '',
            x: '',
            y: '',
            o: ''
        };

    for(var i = 0; i < numCubes; i++) {
        if (parent.attr('class').indexOf('slick-slide') > -1) {
            enX = Math.floor((Math.random() * (parent.width() * 20 / 100)) + 0);
        } else {
            enX = Math.floor((Math.random() * parent.width()) + 0);
        }

        enY = Math.floor((Math.random() * parent.height() - 40) + 0);

        WH = Math.floor((Math.random() * 150) + 60);
        params.h = WH;
        params.w = params.h;
        params.x = enX;
        params.y = enY;

        console.log('enX antes', enX);
        compruebaXY(enX, enY);
        console.log('enX despues', enX);

        parent.append('<div class="flex-item cube cube' + i + '" style="z-index: 10; background: #fff; position: absolute; width: ' + params.w + 'px; height: ' + params.h + 'px; top: ' + params.y + 'px; left: ' + params.x + 'px;"></div>');
        arrayCubes.push($('.cube' + i));
    }

    animaCubes(arrayCubes);
}

function compruebaXY(x, y) {
    $('.cube').each(function () {
        if (x < $(this).offset().left + $(this).width()) {
            x = Math.floor($(this).offset().left + $(this).width());

            enX = x;
        }

        if (y < $(this).offset().top + $(this).height()) {
            y = Math.floor($(this).offset().top + $(this).height());

            enY = y;
        }
    });
}

function deleteCubes(parent) {
    TweenMax.to(parent.find('.cube'), tc,{ opacity: 0, onComplete: function () {
        parent.find('.cube').remove();
    } });
}

function animaCubes(cubes) {
    TweenMax.staggerFromTo(cubes, 0.1, { opacity: 0 }, { opacity: Math.random() * 0.2 + 0.1 }, 0.5);
}
// Detectar dispositivo móvil
window.mobilecheck = function() {
	var check = false;
	(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);

	// Lógica
	if(check){
		is_mobile();
	} else{
		isnt_mobile();
	}

	return check;
}

// Si es móvil...
function is_mobile(){

	addClass(page.html, 'mobile');
	removeClass(page.html, 'not-mobile');

}

// Si no lo es...
function isnt_mobile(){

	addClass(page.html, 'not-mobile');
	removeClass(page.html, 'mobile');

}
function setup_DOM(){
	page = {};
	page.html = $('html');
	page.head = $('head');
	page.burguer = $('.burguer button');
	page.burguer.icon = $('.burguer button i');
	page.equal = $('.equalheight > *');
}
// Igualar alto de elementos en la misma fila
equalheight = function(container){

	var currentTallest = 0,
		currentRowStart = 0,
		rowDivs = new Array(),
		$el,
		topPosition = 0;

	$(container).each(function() {

		$el = $(this);
		$($el).height('auto')
		topPostion = $el.position().top;

		if (currentRowStart != topPostion) {
			for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
				rowDivs[currentDiv].height(currentTallest);
			}
			rowDivs.length = 0; // empty the array
			currentRowStart = topPostion;
			currentTallest = $el.height();
			rowDivs.push($el);
		} else {
			rowDivs.push($el);
			currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
			currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
		}
		for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
			rowDivs[currentDiv].height(currentTallest);
		}
	});
}
// Comprobar carga de JS
function jscheck(){
	removeClass(page.html, 'no-js');
	addClass(page.html, 'js');
}
var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;

launchSlick = function(){
	var numSlides = 4,
		numSlides2 = 2, 
		numColumn = 2;
	if(mobilecheck()) {
		numSlides = 1;
		numSlides2 = 1;
	}

    // Para seliders sencillos, meter aquí... $('.slick-desktop, .otro-slider, ...')
	var sliders = $('.slick-desktop'),
		slidersMulti = $('.menu-options-slider'),
		itemsMenu = $('.menu-options-slider > *');

    if(mobilecheck()) {
        slidersMulti.slick({
        	dots: false,
			arrows: true,
			slidesToShow: 3
		});
    }

    // Configuración | Slider de portada
	sliders.slick({
		dots: true,
		dotsClass: 'slick-desktop__dots flex-container',
		arrows: true
	});

	// Añadimos cubos
	if (!mobilecheck()) {
        addCubes($('.slick-desktop .slick-slide.slick-current'));
    }

	// Navegación personalizada
	customDots();

	sliders.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
		if (!mobilecheck()) {
            // Borramos todos los cubos
            $('.slick-desktop .slick-slide .cube').remove()
            // Añadimos cubos
            addCubes($('.slick-desktop .slick-slide.slick-current').next());
            addCubes($('.slick-desktop .slick-slide.slick-current').prev());
            addCubes($('.slick-desktop .slick-slide.slick-cloned').prev());
        }
	});
};

customDots = function () {
    var arrayTexts = [],
        text;

    $('.slick-desktop [class*="__item"]').each(function () {
        text = $(this).find('h2').html();
        arrayTexts.push(text);
    });

    for(var i = 1; i < $('.slick-desktop__dots li').length + 1; i++) {
        $('.slick-desktop__dots li:nth-child(' + i + ')').html(arrayTexts[i]);
    }
};
var sliderHome = $('.m-portada-slider'),
    headerH = $('.header .contenedor:last-child').height() - 5;

sliderHome.css('margin-top', headerH);
$(window).resize(function () {
    headerH = $('.header').height();
    sliderHome.css('margin-top', headerH);
});
// Se lanza cuando se ha cargado la página
window.onload = function(){

	// Configuramos los elementos del DOM
	setup_DOM();

	// Configuramos JS como cargado
	jscheck();

	// Detectamos si el acceso se produce desde desde dispositivo móvil
	mobilecheck();

	// Elimina la clase de carga
	removeClass(page.html, 'loading');

	// Lanza Slick
	launchSlick();

	// Iguala hijos directos de elementos con clas .equalheight
	equalheight(page.equal);
};

// Se lanza al redimensionar la ventana
$(window).resize(function(){
	equalheight(page.equal);
	mobilecheck();
});