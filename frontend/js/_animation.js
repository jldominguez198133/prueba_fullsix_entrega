// Vars principal
var count = 0,
    tc = 0.5,
    pNeg = '-=20',
    pPos = '+=20',
    int = 0.2,
    buffer = $('.spinner-content'),
    card = $('.tarjeta'),
    secVentajas = $('.ventajas'),
    secVentajasH = secVentajas.height(),
    modsVentajas = $('.ventajas .flex-container > *'),
    TLMuestra;

// Animación Buffer
var TLBuffer = new TimelineMax();
    TLBuffer
        .to(buffer, tc, { opacity: 0, display: 'none', delay: 2, onComplete: function () {
            if (!mobilecheck() && $(window).width() > 768) {
                // Colocamos elementos
                TweenMax.set(modsVentajas, {marginTop: secVentajasH});
                // Comienza animación
                compruebaPos();
            }
        } });

// Animación tarjeta
// Evento click sobre el elemento tarjeta
card.click(function () {
    if (!mobilecheck() && $(window).width() > 768) {
        if ($(this).attr('class').indexOf('fixed') > -1) {
            console.log('card')
            TweenMax.to($('html, body'), tc, {scrollTop: secVentajas.offset().top});
        }
    }
});

function compruebaPos() {

    if (card.offset().top >= secVentajas.offset().top - 48) {
        card.removeClass('fixed');
        card.addClass('no-fixed');
        showMods();
    }

    $(window).scroll(function () {
        if (card.offset().top >= secVentajas.offset().top - 48) {
            card.removeClass('fixed');
            card.addClass('no-fixed');
            if (count == 0) {
                showMods();
            }
        }

        if ($(window).scrollBottom() > $('footer').height() + secVentajasH + 90) {
            card.removeClass('no-fixed');
            card.addClass('fixed');
            hideMods();
        }
    });
}

function showMods() {
    TLMuestra = new TimelineMax({});

    TLMuestra
        .staggerFromTo(modsVentajas, tc, { opacity: 0, marginTop: secVentajasH }, { opacity: 1, marginTop: 0, ease: Back.easeOut }, int);

    count++;
}
function hideMods() {
    modsVentajas.css('margin-top', secVentajasH);
    count = 0;
}

$.fn.scrollBottom = function() {
    return $(document).height() - this.scrollTop() - this.height();
};

// Animación cuadrados hover
var elemsAnimHover = $('.destacados [class*="__img"]'),
    numCubes = 6;

elemsAnimHover.each(function () {
    $(this).mouseenter(function() {
        addCubes($(this));
    });
    $(this).mouseleave(function() {
        deleteCubes($(this));
    });
});

var enX,
    enY;

function addCubes(parent) {
    //if (parent.attr('class').indexOf('slick-slide') > -1) numCubes = 8;

    var WH,
        arrayCubes = []
        params = {
            h: '',
            w: '',
            x: '',
            y: '',
            o: ''
        };

    for(var i = 0; i < numCubes; i++) {
        if (parent.attr('class').indexOf('slick-slide') > -1) {
            enX = Math.floor((Math.random() * (parent.width() * 20 / 100)) + 0);
        } else {
            enX = Math.floor((Math.random() * parent.width()) + 0);
        }

        enY = Math.floor((Math.random() * parent.height() - 40) + 0);

        WH = Math.floor((Math.random() * 150) + 60);
        params.h = WH;
        params.w = params.h;
        params.x = enX;
        params.y = enY;

        console.log('enX antes', enX);
        compruebaXY(enX, enY);
        console.log('enX despues', enX);

        parent.append('<div class="flex-item cube cube' + i + '" style="z-index: 10; background: #fff; position: absolute; width: ' + params.w + 'px; height: ' + params.h + 'px; top: ' + params.y + 'px; left: ' + params.x + 'px;"></div>');
        arrayCubes.push($('.cube' + i));
    }

    animaCubes(arrayCubes);
}

function compruebaXY(x, y) {
    $('.cube').each(function () {
        if (x < $(this).offset().left + $(this).width()) {
            x = Math.floor($(this).offset().left + $(this).width());

            enX = x;
        }

        if (y < $(this).offset().top + $(this).height()) {
            y = Math.floor($(this).offset().top + $(this).height());

            enY = y;
        }
    });
}

function deleteCubes(parent) {
    TweenMax.to(parent.find('.cube'), tc,{ opacity: 0, onComplete: function () {
        parent.find('.cube').remove();
    } });
}

function animaCubes(cubes) {
    TweenMax.staggerFromTo(cubes, 0.1, { opacity: 0 }, { opacity: Math.random() * 0.2 + 0.1 }, 0.5);
}