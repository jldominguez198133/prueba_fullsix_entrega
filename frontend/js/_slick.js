var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;

launchSlick = function(){
	var numSlides = 4,
		numSlides2 = 2, 
		numColumn = 2;
	if(mobilecheck()) {
		numSlides = 1;
		numSlides2 = 1;
	}

    // Para seliders sencillos, meter aquí... $('.slick-desktop, .otro-slider, ...')
	var sliders = $('.slick-desktop'),
		slidersMulti = $('.menu-options-slider'),
		itemsMenu = $('.menu-options-slider > *');

    if(mobilecheck()) {
        slidersMulti.slick({
        	dots: false,
			arrows: true,
			slidesToShow: 3
		});
    }

    // Configuración | Slider de portada
	sliders.slick({
		dots: true,
		dotsClass: 'slick-desktop__dots flex-container',
		arrows: true
	});

	// Añadimos cubos
	if (!mobilecheck()) {
        addCubes($('.slick-desktop .slick-slide.slick-current'));
    }

	// Navegación personalizada
	customDots();

	sliders.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
		if (!mobilecheck()) {
            // Borramos todos los cubos
            $('.slick-desktop .slick-slide .cube').remove()
            // Añadimos cubos
            addCubes($('.slick-desktop .slick-slide.slick-current').next());
            addCubes($('.slick-desktop .slick-slide.slick-current').prev());
            addCubes($('.slick-desktop .slick-slide.slick-cloned').prev());
        }
	});
};

customDots = function () {
    var arrayTexts = [],
        text;

    $('.slick-desktop [class*="__item"]').each(function () {
        text = $(this).find('h2').html();
        arrayTexts.push(text);
    });

    for(var i = 1; i < $('.slick-desktop__dots li').length + 1; i++) {
        $('.slick-desktop__dots li:nth-child(' + i + ')').html(arrayTexts[i]);
    }
};